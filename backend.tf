terraform {
  backend "s3" {
    bucket = "rootmeet-us-west-1-s3" # Replace with your actual S3 bucket name
    key    = "Gitlab/terraform.tfstate"
    region = "us-west-1"
  }
}
